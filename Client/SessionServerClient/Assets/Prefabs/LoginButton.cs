using Firebase.Auth;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Prefabs
{
    public class LoginButton : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private TMP_InputField email;
        [SerializeField] private TMP_InputField password;

        // Start is called before the first frame update
        void Start()
        {
            button.onClick.AddListener(() =>
            {
                Debug.Log(email.text);
                Debug.Log(password.text);

                this.SignInWithEmailAndPassword();
            });
        }

        async void SignInWithEmailAndPassword()
        {
            var user = await FirebaseAuth.DefaultInstance.SignInWithEmailAndPasswordAsync(email.text, password.text);

            FirebaseManager.Instance.CurrentUser = user;
            
            Debug.Log(user);
        }
    }
}
