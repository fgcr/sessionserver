using System;
using Firebase.Auth;
using UnityEngine;

namespace Prefabs
{
    public class FirebaseManager : MonoBehaviour
    {
        [NonSerialized] public static FirebaseManager Instance;

        [NonSerialized] public FirebaseAuth auth;

        [NonSerialized] public FirebaseUser CurrentUser;

        private void Awake()
        {
            Instance = this;

            auth = FirebaseAuth.DefaultInstance;

            auth.StateChanged += AuthStateChanged;
        }
        
        void AuthStateChanged(object sender, System.EventArgs eventArgs) {
            if (auth.CurrentUser != CurrentUser) {
                bool signedIn = CurrentUser != auth.CurrentUser && auth.CurrentUser != null;
                if (!signedIn && CurrentUser != null) {
                    Debug.Log("Signed out " + CurrentUser.UserId);
                }
                CurrentUser = auth.CurrentUser;
                if (signedIn) {
                    Debug.Log("Signed in " + CurrentUser.UserId);
                }
            }
        }

        void OnDestroy() {
            auth.StateChanged -= AuthStateChanged;
            auth = null;
        }
    }
}
