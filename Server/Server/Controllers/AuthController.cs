﻿using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Singletons;
using Server.Policies.AccessLevel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [ApiController]
    public class AuthController : ControllerBase
    {
        private FirebaseAuth _firebaseAuth;
        private CommonJsonResultFactory _commonJsonResultFactory;
        private const string sessionCookieName = "session";
        private const string hasSessionCookieName = "hasSession";

        public AuthController(FirebaseAdminManager firebaseAdminManager, CommonJsonResultFactory commonJsonResultFactory)
        {
            this._firebaseAuth = firebaseAdminManager.GetFirebaseAuth();
            this._commonJsonResultFactory = commonJsonResultFactory;
        }

        [Route("/Auth/SessionLogin")]
        [HttpPost]
        public async Task<JsonResult> SessionLogin([FromBody] string idToken)
        {
            if(Request.Cookies.ContainsKey(sessionCookieName) == true)
            {
                return new JsonResult(true);
            }

            var options = new SessionCookieOptions()
            {
                ExpiresIn = TimeSpan.FromDays(1),
            };

            try
            {
                var decodedToken = await FirebaseAuth.DefaultInstance.VerifyIdTokenAsync(idToken);

                var loginUser = await this._firebaseAuth.GetUserAsync(decodedToken.Uid);

                //Create Claim
                if((decodedToken.Claims == null) || (decodedToken.Claims.ContainsKey(nameof(AccessLevel)) == false))
                {
                    var newClaims = new Dictionary<string, object>();

                    if (loginUser.EmailVerified)
                    {
                        newClaims.Add(nameof(AccessLevel), (int)AccessLevel.VerifiedUser);
                    }
                    else
                    {
                        newClaims.Add(nameof(AccessLevel), (int)AccessLevel.NonVerifiedUser);
                    }

                    await this._firebaseAuth.SetCustomUserClaimsAsync(decodedToken.Uid, newClaims);
                }

                var sessionCookie = await _firebaseAuth
               .CreateSessionCookieAsync(idToken, options);

                // Set cookie policy parameters as required.
                var secureCookieOptions = new CookieOptions()
                {
                    Expires = DateTimeOffset.UtcNow.Add(options.ExpiresIn),
                    HttpOnly = true,
                    Secure = true,
                };

                this.Response.Cookies.Append(sessionCookieName, sessionCookie, secureCookieOptions);

                var cookieOptions = new CookieOptions()
                {
                    Expires = DateTimeOffset.UtcNow.Add(options.ExpiresIn),
                    HttpOnly = false,
                    Secure = true,
                };

                this.Response.Cookies.Append(hasSessionCookieName, "true", cookieOptions);

                return new JsonResult(true);
            }
            catch (FirebaseAuthException)
            {
                return (JsonResult)this._commonJsonResultFactory.CreateCommonJsonResult(CommonJsonResultFactory.CommonJsonResultType.InternalError);
            }
        }

        [Route("/Auth/SessionLogout")]
        [CheckAccessLevel(AccessLevel.NonVerifiedUser)]
        [HttpPost]
        public async Task<JsonResult> SessionLogout()
        {
            try
            {
                var uid = this.Request.Headers["uid"].ToString();

                await _firebaseAuth.RevokeRefreshTokensAsync(uid);

                this.Response.Cookies.Delete(sessionCookieName);

                this.Response.Cookies.Delete(hasSessionCookieName);

                return new JsonResult(true);
            }
            catch (ArgumentException)
            {
                return this._commonJsonResultFactory.CreateCommonJsonResult(CommonJsonResultFactory.CommonJsonResultType.BadRequest);
            }
            catch (FirebaseAuthException)
            {
                return this._commonJsonResultFactory.CreateCommonJsonResult(CommonJsonResultFactory.CommonJsonResultType.Unauthorized);
            }
        }
    }
}
