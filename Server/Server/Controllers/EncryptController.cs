﻿using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Server.Singletons;

namespace Server.Controllers
{
    public class EncryptController
    {
        private EncryptManager _encryptManager;

        public EncryptController(EncryptManager encryptManager)
        {
            this._encryptManager = encryptManager;
        }

        [Route("/Encrypt/GetPublicKey")]
        [HttpGet]
        public JsonResult GetPublicKey()
        {
            var publicKey = this._encryptManager.GetPublicKey();

            var publicKeyString = Convert.ToBase64String(publicKey);

            return new JsonResult(publicKeyString);
        }

        [Route("/Encrypt/SendEncrypted")]
        [HttpPost]
        public JsonResult SendEncrypted(string json)
        {
            var jsonBytes = Encoding.Default.GetBytes(json);

            var decryptedJsonBytes = _encryptManager.Decrypt(jsonBytes);

            var decryptedJsonString = Encoding.Default.GetString(decryptedJsonBytes);

            return new JsonResult(true);
        }
    }
}
