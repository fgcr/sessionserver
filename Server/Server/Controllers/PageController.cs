﻿using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [ApiController]
    public class PageController : ControllerBase
    {
        [Route("{Page=Home}")]
        public IActionResult Main()
        {
            return File("index.html", "text/html");
        }
    }
}
