﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Server.Policies.AccessLevel;
using Server.Singletons;
using Server.Models.User;
using Server.Models;
using Microsoft.EntityFrameworkCore;

namespace Server.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private CommonJsonResultFactory _resultFactory;
        private RandomManager _random;

        public UserController(CommonJsonResultFactory resultFactory, RandomManager random)
        {
            this._resultFactory = resultFactory;
            this._random = random;
        }

        [Route("/User/GetUserInfo")]
        [CheckAccessLevel(AccessLevel.NonVerifiedUser)]
        [HttpGet]
        public async Task<JsonResult> GetUserInfo()
        {
            var uid = this.ControllerContext.HttpContext.Request.Headers["uid"][0];

            using (var userInfo = new UserInfoContext(new DbContextOptions<UserInfoContext>()))
            {
                try
                {
                    await userInfo.Database.EnsureCreatedAsync();

                    var targetUser = await userInfo.FindAsync<UserInfo>(uid);

                    if (targetUser != null)
                    {
                        return new JsonResult(targetUser);
                    }

                    var newTargetUserNickname = $"guest{this._random.GetRandomString(12)}";

                    var newTargetUser = new UserInfo()
                    {
                        Uid = uid,
                        Nickname = newTargetUserNickname
                    };

                    await userInfo.AddAsync(newTargetUser);

                    await userInfo.SaveChangesAsync();

                    return new JsonResult(newTargetUser);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    return this._resultFactory.CreateCommonJsonResult(CommonJsonResultFactory.CommonJsonResultType.InternalError);
                }
            }
        }

        [Route("/User/GetVerifiedUserInfo")]
        [CheckAccessLevel(AccessLevel.VerifiedUser)]
        [HttpGet]
        public JsonResult GetVerifiedUserInfo()
        {
            return new JsonResult(true);
        }
    }
}
