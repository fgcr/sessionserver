﻿using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;
using Microsoft.Extensions.Options;
using System.Text.Json;

namespace Server.Singletons
{
    public class FirebaseAdminManager
    {
        private FirebaseApp _firebaseApp;
        private FirebaseAuth _firebaseAuth;
        public FirebaseAdminManager(IOptions<FirebaseConfig> firebaseConfig)
        {
            var credentialJson = JsonSerializer.Serialize(firebaseConfig.Value.credential);

            this._firebaseApp = FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromJson(credentialJson)
            });

            this._firebaseAuth = FirebaseAuth.GetAuth(this._firebaseApp);
        }

        public FirebaseAuth GetFirebaseAuth()
        {
            return this._firebaseAuth;
        }
    }
}
