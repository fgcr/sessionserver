﻿using System.Security.Cryptography;

namespace Server.Singletons
{
    public class EncryptManager
    {
        private RSA _rsa;
        private RSAParameters _rsaParameters;

        public EncryptManager()
        {
            this._rsa = RSA.Create();
            this._rsaParameters = this._rsa.ExportParameters(true);
        }

        public byte[] GetPublicKey()
        {
            return this._publicKey;
        }

        public byte[] Decrypt(byte[] value)
        {
            return this._rsa.Decrypt(value, RSAEncryptionPadding.OaepSHA256);
        }
    }
}
