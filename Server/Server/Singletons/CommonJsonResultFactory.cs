﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Singletons
{
    public class CommonJsonResultFactory
    {
        public enum CommonJsonResultType
        {
            Unauthorized,
            BadRequest,
            InternalError
        }

        public JsonResult CreateCommonJsonResult(CommonJsonResultType commonJsonResultType)
        {
            switch (commonJsonResultType)
            {
                case CommonJsonResultType.Unauthorized:
                    return new JsonResult(new
                    {
                        Message = "Session is not validated. Request Access Denied.",
                    })
                    {
                        StatusCode = StatusCodes.Status401Unauthorized,
                    };

                case CommonJsonResultType.InternalError:
                    return new JsonResult(new
                    {
                        Message = "Internal Server Error."
                    })
                    {
                        StatusCode = StatusCodes.Status500InternalServerError
                    };

                case CommonJsonResultType.BadRequest:
                    return new JsonResult(new
                    {
                        Message = "Received bad request."
                    })
                    {
                        StatusCode = StatusCodes.Status400BadRequest
                    };

                default:
                    return new JsonResult(new
                    {
                        Message = "No such common json result type exists."
                    })
                    {
                        StatusCode = StatusCodes.Status500InternalServerError
                    };
            }
        }
    }
}
