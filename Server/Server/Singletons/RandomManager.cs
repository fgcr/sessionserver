﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Singletons
{
    public class RandomManager
    {
        private Random _random;
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        public RandomManager()
        {
            this._random = new Random();
        }

        public int GetNextInt(int max)
        {
            return this._random.Next(max);
        }

        public int GetNextInt(int min, int max)
        {
            return this._random.Next(min, max);
        }

        public string GetRandomString(int length)
        {
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[this._random.Next(s.Length)]).ToArray());
        }
    }
}
