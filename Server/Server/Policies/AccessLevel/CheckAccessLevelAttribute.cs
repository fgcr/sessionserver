﻿using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Server.Singletons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Server.Policies.AccessLevel
{
    public enum AccessLevel
    {
        None,
        NonVerifiedUser,
        VerifiedUser,
        Administrator
    }

    public class CheckAccessLevelAttribute : TypeFilterAttribute
    {
        public CheckAccessLevelAttribute(AccessLevel accessLevel)
            : base(typeof(AccessLevelFilter))
        {
            Arguments = new object[] { accessLevel };
        }
    }

    /// <summary>
    /// This filter checks authorization and save firebase userdata
    /// </summary>
    public class AccessLevelFilter : IAsyncAuthorizationFilter
    {
        private readonly AccessLevel _accessLevel;
        private readonly FirebaseAuth _firebaseAuth;
        private readonly CommonJsonResultFactory _resultFactory;

        private FirebaseToken _firebaseToken;

        public AccessLevelFilter(
            AccessLevel accessLevel, 
            FirebaseAdminManager firebaseAdminManager, 
            CommonJsonResultFactory commonJsonResultFactory
            )
        {
            this._accessLevel = accessLevel;
            this._firebaseAuth = firebaseAdminManager.GetFirebaseAuth();
            this._resultFactory = commonJsonResultFactory;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var sessionCookie = context.HttpContext.Request.Cookies["session"];

            if (string.IsNullOrEmpty(sessionCookie))
            {
                context.Result = _resultFactory.CreateCommonJsonResult(CommonJsonResultFactory.CommonJsonResultType.Unauthorized);
                return;
            }

            try
            {
                var checkRevoked = true;
                var decodedToken = await this._firebaseAuth.VerifySessionCookieAsync(
                    sessionCookie, checkRevoked);

                if(decodedToken.Claims.ContainsKey(nameof(AccessLevel)) == false)
                {
                    context.Result = _resultFactory.CreateCommonJsonResult(CommonJsonResultFactory.CommonJsonResultType.Unauthorized);
                    return;
                }

                object accessLevelRaw = decodedToken.Claims[nameof(AccessLevel)];

                var accessLevelLong = (long)accessLevelRaw;

                var accessLevel = (AccessLevel)accessLevelLong;

                if (accessLevel < this._accessLevel)
                {
                    context.Result = _resultFactory.CreateCommonJsonResult(CommonJsonResultFactory.CommonJsonResultType.Unauthorized);
                    return;
                }

                this._firebaseToken = decodedToken;

                context.HttpContext.Request.Headers.Add("uid", decodedToken.Uid);
            }
            catch (FirebaseAuthException)
            {
                context.Result = _resultFactory.CreateCommonJsonResult(CommonJsonResultFactory.CommonJsonResultType.Unauthorized);
                return;
            }
        }
    }
}
