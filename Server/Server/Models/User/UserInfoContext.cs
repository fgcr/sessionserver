﻿using System.Configuration;
using Microsoft.EntityFrameworkCore;


namespace Server.Models.User
{
    public class UserInfoContext : DbContext
    {
        public DbSet<UserInfo> UserInfo { get; set; } 

        public UserInfoContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserInfo>().HasKey(e => e.Uid);
            modelBuilder.Entity<UserInfo>().Property(e => e.Nickname);
        }
    }

    public class UserInfo
    {
        public string Uid { get; set; }
        public string Nickname { get; set; }
    }
}
