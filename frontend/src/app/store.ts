import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { encryptReducer } from '../features/encrypt/EncryptSlice';
import { firebaseReducer } from '../features/session/FirebaseSlice';

export const store = configureStore({
  reducer: {
    CookieReducer : firebaseReducer,
    EncryptReducer : encryptReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
