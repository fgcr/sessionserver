export enum PageType {
    Main,
    SignIn,
    CreateUser,
    EncryptTest,
}

export function GetPath(...paths : PageType[]) : string{
    let resultUrl = "/";
  
    for(let path of paths){
      resultUrl += `${PageType[path]}/`
    }
  
    return resultUrl
  };
  
  export function GetAppendingPath(...paths : PageType[]) : string{
    let resultUrl = "";
  
    for(let path of paths){
      resultUrl += `${PageType[path]}/`
    }
  
    return resultUrl
  };