import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface FirebaseState{
  isLogin : boolean
}

const initialState : FirebaseState = {
  isLogin : false
}

export const FirebaseSlice = createSlice({
  name : "sessionSlice",
    initialState,
    reducers:{
      SetLoginState : (state, action : PayloadAction<boolean>) => {

        state.isLogin = action.payload;

      }
    },
})

export const { SetLoginState } = FirebaseSlice.actions;

export const firebaseReducer = FirebaseSlice.reducer;