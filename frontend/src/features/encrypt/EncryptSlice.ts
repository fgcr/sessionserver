import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { GetPublicKey } from "../../api/encryptApi";
import { Encryptor } from "../../app/encryptor";

export interface EncryptState{
  publicKey : string | undefined
}

const initialState : EncryptState = {
    publicKey : undefined
}

export const GetPublicKeyThunk = createAsyncThunk(
    'encrypt/getpublickey',
    async () => {
        let result = await GetPublicKey();

        let resultJson = await result.json();

        return resultJson;
    }
)

export const EncryptSlice = createSlice({
  name : "sessionSlice",
    initialState,
    reducers:{

    },
    extraReducers : (builders) => {
        builders
        .addCase(GetPublicKeyThunk.fulfilled, (state, action : PayloadAction<string>) => {
            state.publicKey = action.payload;
            console.info("Setting public key was successful.");
            Encryptor.setPublicKey(state.publicKey);
            
        })
        .addCase(GetPublicKeyThunk.rejected, (state) => {
            state.publicKey = undefined;
            console.error("Cannot get public key from server.");
            Encryptor.setPublicKey("");
        })
    }
})

export const encryptReducer = EncryptSlice.reducer;