import React, { useRef } from "react";
import { SendEncryptedJson } from "../../api/encryptApi";
import { Encryptor } from "../../app/encryptor";
import { useAppSelector } from "../../app/hooks";
import { EncryptState } from "../encrypt/EncryptSlice";

export function EncryptTest() : React.ReactElement{
    
    const EncryptState = useAppSelector<EncryptState>(state => state.EncryptReducer);

    const value1 = useRef<string>();
    const value2 = useRef<string>();

    return (
        <div>
            <div>Value1</div>
            <input onChange={(ev) => {
                value1.current = ev.target.value;
            }}></input>
            <div>Value2</div>
            <input onChange={(ev) => {
                value2.current = ev.target.value;
            }}></input>
            <button onClick={(ev) => {
                async function requestEncryptedJson(){
                    let sendTarget = {
                        value1 : value1.current,
                        value2 : value2.current
                    }
    
                    let sendTargetJson = JSON.stringify(sendTarget);
    
                    if(EncryptState.publicKey === undefined){
                        console.error("public key is not defined.");
                        return;
                    }
    
                    const encryptedJson = Encryptor.encrypt(sendTargetJson);

                    if(encryptedJson === false){
                        console.error("cannot encrypt data.");
                        return;
                    }

                    await SendEncryptedJson(encryptedJson);
                }
                
                requestEncryptedJson();
            }}>Request</button>
        </div>
    )
}