import { ActionCodeSettings, sendEmailVerification, signOut } from "firebase/auth";
import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { GetUserInfo, GetVerifiedUserInfo } from "../../api/userApi";
import { auth } from "../../app/firebase";
import { useAppSelector } from "../../app/hooks";
import { GetPath, PageType } from "../../app/pages";
import { FirebaseState } from "../session/FirebaseSlice";

export function Main() : React.ReactElement {

    const firebaseState = useAppSelector<FirebaseState>(state => state.CookieReducer);

    useEffect(() => {

    }, [firebaseState])

    return (
        <div>
            <div>This is Main page</div>
            {
                firebaseState.isLogin ?
                <div>
                    <button onClick={(ev) => {
                        async function getAsync(){
                            let result = await GetUserInfo();
                            
                            console.log(result);
                        }

                        getAsync();
                    }}>User Info Request</button>
                    <button onClick={(ev) => {
                        async function getAsync(){
                            let result = await GetVerifiedUserInfo();

                            console.log(result);
                        }

                        getAsync();
                    }}>Verified User Info Request</button>
                    <button onClick={(ev) => {
                        async function getAsync(){
                            await signOut(auth);
                        }

                        getAsync();
                    }}>Log Out</button>
                </div>
                : <div>
                    <Link to={GetPath(PageType.SignIn)}>Sign In</Link>
                    <br></br>
                    <Link to={GetPath(PageType.CreateUser)}>Create User</Link>
                </div>
            }
            {
                ((firebaseState.isLogin === true) 
                && (auth.currentUser?.emailVerified === false)) ?
                <VerifyEmail/>
                : <></>
            }
            {
                <Link to={GetPath(PageType.EncryptTest)}>Encrypt Test</Link>
            }
        </div>
    )
}

function VerifyEmail() : React.ReactElement {

    return (
        <div>
            <button onClick={(ev) => {
                async function verifyEmail(){
                    let actionCodeSettings : ActionCodeSettings = {
                        url : "http://localhost:5000/",
                        handleCodeInApp : true
                    };

                    if(auth.currentUser && auth.currentUser.email){
                        await sendEmailVerification(auth.currentUser, actionCodeSettings);
                    }
                }

                verifyEmail();
            }}>Verify Email</button>
        </div>
    )
}