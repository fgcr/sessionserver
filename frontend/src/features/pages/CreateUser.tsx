import { createUserWithEmailAndPassword, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import React, { useRef, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { auth } from "../../app/firebase";
import { useAppSelector } from "../../app/hooks";
import { GetPath, PageType } from "../../app/pages";
import { FirebaseState } from "../session/FirebaseSlice";

export function CreateUser() : React.ReactElement {

    const firebaseState = useAppSelector<FirebaseState>(state => state.CookieReducer);

    const navigate = useNavigate();

    const email = useRef<string>("");
    const pwd = useRef<string>("");

    useEffect(()=> {
        if(firebaseState.isLogin){
            navigate(GetPath(PageType.Main));
        }
    }, [firebaseState, navigate])

    return (
        <div>
            <div>This is CreateUser Page</div>
            <div>Email</div>
            <input onChange={(ev) => {
                email.current = ev.target.value;
            }} placeholder="Email Address"></input>
            <div>Password</div>
            <input type={"password"} onChange={(ev) => {
                pwd.current = ev.target.value;
            }} placeholder="Password"></input>
            <button onClick={(ev) => {
                async function createUser(){
                    await createUserWithEmailAndPassword(auth, email.current, pwd.current);
                }
                
                createUser();
            }}>Create User</button>
            <button onClick={(ev) => {
                async function googleAuth(){
                    await signInWithPopup(auth, new GoogleAuthProvider());
                }
                
                googleAuth();
            }}>Google</button>
            <div>
                <Link to={GetPath(PageType.Main)}>Back To Main</Link>
            </div>
            
        </div>
    )
}