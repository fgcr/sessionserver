export async function GetPublicKey() {
    return await fetch(
        "/Encrypt/GetPublickey",
        {
            method : "get",
            mode : "cors",
            cache : "no-cache",
            headers : {
                'Content-Type' : 'application/json'
            },
        });
}

export async function SendEncryptedJson(json : string) {
    return await fetch(
        "/Encrypt/SendEncrypted",
        {
            method : "post",
            mode : "cors",
            cache : "no-cache",
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(json)
        });
}