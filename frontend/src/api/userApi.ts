export async function GetUserInfo() {
  return await fetch(
      "/User/GetUserInfo",
      {
          method : "get",
          mode : "cors",
          cache : "no-cache",
          headers : {
              'Content-Type' : 'application/json'
          },
      });
}

export async function GetVerifiedUserInfo() {
    return await fetch(
        "/User/GetVerifiedUserInfo",
        {
            method : "get",
            mode : "cors",
            cache : "no-cache",
            headers : {
                'Content-Type' : 'application/json'
            },
        });
}