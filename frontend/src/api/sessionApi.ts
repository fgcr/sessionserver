export async function SessionLogin(idToken : string) {

  return await fetch(
      "/Auth/SessionLogin",
      {
          method : "post",
          mode : "cors",
          cache : "no-cache",
          headers : {
              'Content-Type' : 'application/json'
          },
          body : JSON.stringify(idToken)
      });
}

export async function SessionLogout() {

    return await fetch(
        "/Auth/SessionLogout",
        {
            method : "post",
            mode : "cors",
            cache : "no-cache",
            headers : {
                'Content-Type' : 'application/json'
            },
        });
}