import { onAuthStateChanged, User } from 'firebase/auth';
import React, { useEffect } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import { auth } from './app/firebase';
import { GetPath, PageType } from './app/pages';
import { CreateUser } from './features/pages/CreateUser';
import { Main } from './features/pages/Main';
import { SignIn } from './features/pages/SignIn';
import { SessionLogin, SessionLogout } from "./api/sessionApi";
import { useAppDispatch } from './app/hooks';
import { SetLoginState } from './features/session/FirebaseSlice';
import Cookies from 'js-cookie';
import { EncryptTest } from './features/pages/EncryptTest';
import { GetPublicKeyThunk } from './features/encrypt/EncryptSlice';

function App() : React.ReactElement {

  const dispatch = useAppDispatch();

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      async function refreshAuthState(user : User | null){
        
        await dispatch(GetPublicKeyThunk());

        var hasSession = Cookies.get("hasSession");

        if(user){
          const idToken = await user.getIdToken();
  
          const sessionLoginResult = await SessionLogin(idToken);

          const sessionLoginResultJson = await sessionLoginResult.json() as boolean;

          if(sessionLoginResultJson){
            dispatch(SetLoginState(true));
          }
        }
        else{
          if(hasSession === undefined){
            return;
          }

          let sessionLogoutResult = await SessionLogout();

          const sessionLogoutResultJson = await sessionLogoutResult.json() as boolean;

          if(sessionLogoutResultJson){
            dispatch(SetLoginState(false));
          }
        }
      }
    
      refreshAuthState(user);
    })
  })
  

  return (
    <div className="App">
      <header className="App-header">
      </header>
      <Routes>
        <Route path="/" element={<Main/>}></Route>
        <Route path={GetPath(PageType.Main)} element={<Main/>}></Route>
        <Route path={GetPath(PageType.SignIn)} element={<SignIn/>}></Route>
        <Route path={GetPath(PageType.CreateUser)} element={<CreateUser/>}></Route>
        <Route path={GetPath(PageType.EncryptTest)} element={<EncryptTest/>}></Route>
      </Routes>
    </div>
  );
}

export default App;
